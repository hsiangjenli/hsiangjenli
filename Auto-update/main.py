'''Reference
This code is base on Ichi-Tsai's medium's code.
- https://medium.com/ichitsai/python-note-web-page-screenshot-開發筆記-793f0d45874d
'''
import pathlib, os, asyncio
from dataclasses import dataclass
from pyppeteer import launch

@dataclass
class ScreenShot:
    INPUT_URL: str
    OUTPUT_FNAME: str
    OUTPUT_DIR: str
    WIDTH_HEIGHT: tuple

    def __post_init__(self):
        self.WIDTH = self.WIDTH_HEIGHT[0]
        self.HEIGHT = self.WIDTH_HEIGHT[1]
        self.OUTPUT_DIR_NAME = os.path.join(self.OUTPUT_DIR, self.OUTPUT_FNAME)
    
    async def __capture(self):
        browser = await launch(args=['--no-sandbox'])
        page = await browser.newPage()
        await page.setViewport(
            {'width': self.WIDTH, 'height': self.HEIGHT}
        )
        await page.goto(self.INPUT_URL)
        await page.screenshot({'path': f'{self.OUTPUT_FNAME}.png'})
        await browser.close()
    
    def Capture(self):
        asyncio.get_event_loop().run_until_complete(
            self.__capture()
        )



if __name__ == '__main__':
    PATH_PARENT = pathlib.Path(__file__).parent
    INPUT_URL = f'file:///{PATH_PARENT}/templates/Image__Name.html'
    screen_shot = ScreenShot(
        INPUT_URL=INPUT_URL, 
        OUTPUT_DIR=PATH_PARENT, 
        OUTPUT_FNAME='./Images/Name__李享紝',
        WIDTH_HEIGHT=(250, 120)
    )
    screen_shot.Capture()

