<div align="center">
    {% include 'README__Author.md' %}
    {% include 'README__Education.md' %}
    {% include 'README__Award.md' %}
    {% include 'README__Social_Media.md' %}
    {% include 'README__Personal_Project.md' %}
</div>