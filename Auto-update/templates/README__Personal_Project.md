<table>
  <thead>
    <tr>
      <th>Year</th>
      <th>Name</th>
      <th>Tags</th>
      <th>Documentation</th>
    </tr>
  </thead>
  <tbody>
{% for pj in Personal_Project %}
<tr>
<td>{{Personal_Project[pj]['Year']}}</td>
<td>{{Personal_Project[pj]['Name']}}</td>
<td>

{% for tag in Personal_Project[pj]['Tags'] %} `{{tag}}` {% endfor %}
</td>
<td style="text-align:center">
{% if Personal_Project[pj]['Documentation'] %}
[🌐]({{Personal_Project[pj]['Documentation']}})
{% endif %}
</td>
</tr>
{% endfor%}
  </tbody>
</table>

