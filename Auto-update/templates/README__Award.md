{% for award in Award %}    
{% if Award[award]['Ranking'] == 1 %}**🥇 {{ Award[award]['Year'] }}-{{ Award[award]['Name']}} 第一名 🥇**    
{% elif Award[award]['Ranking'] == 2 %}**🥈 {{ Award[award]['Year'] }}-{{ Award[award]['Name']}} 第二名 🥈**
{% elif Award[award]['Ranking'] == 3 %}**🥉 {{ Award[award]['Year'] }}-{{ Award[award]['Name']}} 第三名 🥉**
{% else %}**🏅 {{ Award[award]['Year'] }}-{{ Award[award]['Name']}} {{ Award[award]['Ranking']}} 🏅**
{% endif %}{% endfor %}