<div align="center">
    <img width='150px' src='https://gitlab.com/hsiangjenli/hsiangjenli/-/raw/main/Images/Avatar__Vector.png'>

# **Hsiang-Jen Li** <small><small>**RN**</small></small>

<img src='https://gitlab.com/hsiangjenli/hsiangjenli/-/raw/main/Images/Name__CN_EN.png' width='110px'>

🏫 **國立台灣科技大學 工管所（大數據與資訊安全）**  
🎓 **國立高雄科技大學 金融系（2022）**  
**hsiangjenli@gmail.com**  

**🥇 2022-國立高雄科技大學-電機系-人工智慧暨類神經網路聯合專題展 第一名 🥇**  
**🏅 2020-Fintech Space-校園成果展  優選 🏅**

[![](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=GitHub&logoColor=white)](https://github.com/hsiangjenli) [![](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/hsiangjenli) [![](https://img.shields.io/badge/my%20it%20Blog-100000?style=for-the-badge&logo=flask&logoColor=white)](https://hsiangjenli.gitlab.io/) [![](https://img.shields.io/badge/Kaggle-20BEFF?style=for-the-badge&logo=Kaggle&logoColor=white)](https://www.kaggle.com/hsiangjenli)
</div>

<table align="center">
<tr>
  <td align="center"></td>
  <td width="35%"><div align='center'>

### <img src="https://img.icons8.com/color/30/000000/python--v1.png"/> **Python**</div>
---
- **`Web Scarping`**  
    透過 request, bs4 等套件爬取網路資料  
- **`Data Analysis`**  
    運用 pandas、numpy 對資料進行運算、交叉分析
- **`Machine Learning`**  
    使用 scikit learn 進行回歸、決策樹、時間序列分析
- **`Data Visualization`**  
    使用 matplotlib、seaborn、plotly 套件進行視覺化
  </td>
  <td width="35%"><div align='center'>

### <img src="https://img.icons8.com/color/30/000000/mysql-logo.png"/> **Database**</div>
---
- **`結構式資料庫`** : MySQL、SQLite
- **`非結構式資料庫`** : MongoDB
- 搭配Python建置結構與非結構資料庫
  </td>
  <td></td>

</tr>


<tr>
  
  <td colspan="4"><div align="center">

### <img src="https://img.icons8.com/color/30/000000/blueprint.png"/> **個人專案**</div>

| Year | Name                           |  Tags                             | Documentation |
| :--: | :----------------------------: | :-------------------------------- | :---: |
| 2022 | **Frozen-Flask-Personal-Blog-個人部落格** | `Flask` `Frozen-Flask` `Bootstrap` `gitlab page` | [🌐](https://hsiangjenli.gitlab.io/) | 
| 2022 | **NKUST-Django-Public Opinion Analysis-輿情分析網頁** | `Web Scraping` `Django` `MongoDB` `Bootstrap` `plotly.js` `Anue` `TWSE`| [📄 wiki](https://gitlab.com/hsiangjenli/NKUST-1102-Django-POA-Midterm-Project/-/wikis/home)    |  
| 2021 | **TWSE-台灣證券交易所**              |  `Web Scraping`                   | [📄 GitHub](https://github.com/hsiangjenli/Web-Scraping-Challenge/tree/main/TWSE｜台灣證券交易所) | 
| 2020 | **LineBot-股票資訊查詢**        |`Line-Bot` `Heroku` `mplfinance` `yfinance`|[📄 GitHub](https://github.com/hsiangjenli/LineBot-STOCK.tw-Public)|

1. [**2022｜網頁｜Django-Public Opinion Analysis**](https://gitlab.com/hsiangjenli/NKUST-1102-Django-POA-Midterm-Project)
1. [**2021｜爬蟲｜Anue-鉅亨網**](https://gitlab.com/tuxedo-web-scraping/anue)
   - 建置非結構式資料庫(MongoDB)
   - 使用 [_CkipTagger_](https://github.com/ckiplab/ckiptagger)
1. [**2021｜爬蟲｜TWSE-台灣證券交易所**](https://github.com/hsiangjenli/Web-Scraping-Challenge/tree/main/TWSE｜台灣證券交易所)


  </td>

</tr>


</table>

---
<small>

> <a target="_blank" href="https://icons8.com/icon/13441/python">Python icon by Icons8</a>｜<a target="_blank" href="https://icons8.com/icon/UFXRpPFebwa2/mysql-logo">MySQL Logo icon by Icons8</a>｜<a target="_blank" href="https://icons8.com/icon/23885/blueprint">Blueprint icon by Icons8</a>
</small>
  

