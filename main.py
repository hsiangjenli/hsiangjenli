from jinja2 import Environment, FileSystemLoader
import tomlkit

def LoadConfig(file):
    with open(file, mode="rt", encoding="utf-8") as info:
        config = tomlkit.load(info)
    return config

def SetEnvironemnt(folder, template):
    env = Environment(loader=FileSystemLoader(folder))
    template = env.get_template(template)
    return template


if __name__ == '__main__':

    config = LoadConfig("Auto-update/README.toml")
    template = SetEnvironemnt(folder='Auto-update/templates', template='README-TEMPLATE.md')
    output = template.render(
        **config['Author'],
        Education = config['Education'],
        Award = config['Award'],
        Social_Media = config['Social_Media'],
        Personal_Project = config['Personal_Project']
    )

    with open("README.md", "w", encoding='utf-8') as readme:
        readme.write(output)